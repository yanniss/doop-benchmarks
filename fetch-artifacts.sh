#!/usr/bin/env bash

# Fetches the artifacts from artifactory that are not part of the git repo.

function get() {
    mkdir -p ${LOCAL_DIR}
    pushd ${LOCAL_DIR}
    wget "${ART_DIR}/$1" -O "$1"
    popd
}

BENCHMARKS="http://centauri.di.uoa.gr:8081/artifactory/list/Demo-benchmarks"

LOCAL_DIR="android-benchmarks"
ART_DIR="${BENCHMARKS}/Android"
get "Camera2Basic-debug.apk"
get "com.android.chrome.hprof.gz"
get "com.android.chrome_57.0.2987.132-298713212_minAPI24_x86_nodpi_apkmirror.com.apk"
get "com.google.android.apps.translate.hprof.gz"
get "com.google.android.apps.translate_5.8.0.RC11.151331239-58001163_minAPI17_x86_nodpi_apkmirror.com.apk"
get "com.instagram.android.hprof.gz"
get "com.instagram.android_10.5.1-48243323_minAPI16_x86_nodpi_apkmirror.com.apk"
get "com.instagram.android_10.5.1-48243317_minAPI16(armeabi-v7a)(320dpi)_apkmirror.com.apk"
get "com.pinterest.hprof.gz"
get "com.pinterest_6.13.0-613068_minAPI16_x86_nodpi_apkmirror.com.apk"
get "com.steam.photoeditor-1.07-www.APK4Fun.com.apk"
get "com.steam.photoeditor.hprof.gz"
get "com.whatsapp_2.17.79-451676_minAPI14.apk"
get "jackpal.androidterm-1.0.70-71-minAPI4.apk"
get "jackpal.androidterm.hprof.gz"
get "Signal-play-debug-4.12.3.apk"
get "viber_messenger_v6.8.8.5.apk"

LOCAL_DIR="android-benchmarks/fb"
ART_DIR="${BENCHMARKS}/Android/fb"
get "com.facebook.katana_93.0.0.13.69-38316609_minAPI21(x86)(280,360,400,420,480,560,640dpi)_apkmirror.com.apk"

LOCAL_DIR="android-benchmarks/fb-lite"
ART_DIR="${BENCHMARKS}/Android/fb-lite"
get "com.facebook.lite_85.0.0.10.176-96581738_minAPI9(x86)(nodpi)_apkmirror.com.apk"

LOCAL_DIR="android-benchmarks/fb-messenger"
ART_DIR="${BENCHMARKS}/Android/fb-messenger"
get "com.facebook.orca_108.0.0.20.70-51652268_minAPI21.apk"

LOCAL_DUR="android-benchmarks/uber"
ART_DIR="${BENCHMARKS}/Android/uber"
get "com.ubercab_4.208.10002-35343_minAPI16(armeabi-v7a)(nodpi)_apkmirror.com.apk"
